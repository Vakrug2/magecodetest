<?php

namespace App\Http\Controllers;

use App\Models\Signup;
use Illuminate\Http\Request;
use function redirect;
use function view;

class SignupController extends Controller {
    
    public function index() {
        return view('signup');
    }
    
    public function save(Request $request) {
        $signup = new Signup();
        $signup->name = $request->get('name');
        $signup->email = $request->get('email');
        $signup->save();
        
        return redirect()->route('signup')->with('message', 'Saved!');
    }
}
