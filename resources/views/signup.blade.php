@extends('layouts.main')
 
@section('title', 'Sign up')

@section('content')
<a href="{{ route('home') }}">Home</a>

<form method="POST" action="{{ route('signup-save') }}">
    @csrf
    <div class="mb-3">
        <label for="name" class="form-label">Name</label>
        <input id="name" class="form-control" type="text" name="name" />
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input id="email" class="form-control" type="email" name="email" />
    </div>
    <input type="submit" class="btn btn-primary" value="Save" />
</form>

@if (Session::has('message'))
<div class="message mt-2">
    {{ Session::get('message') }}
</div>
@endif
@endsection